using System.Collections;
using UnityEngine;
using Unity.Netcode;

public class PlayerNetwork : NetworkBehaviour
{
    public NetworkVariable<bool> isSeeker = new NetworkVariable<bool>(false);

    private Rigidbody rb;
    public float moveSpeed = 3f;
    public float seekerSpeed = 12f;
    public float runnerSpeed = 10f;
    private float originalSeekerSpeed;
    private float originalRunnerSpeed;

    private bool isGameStarted = false;
    private float rotationThreshold = 0.1f;

    private void Start()
    {
        if (IsOwner)
        {
            rb = GetComponent<Rigidbody>();
            rb.interpolation = RigidbodyInterpolation.Interpolate;
        }
        originalSeekerSpeed = seekerSpeed;
        originalRunnerSpeed = runnerSpeed;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        DontDestroyOnLoad(gameObject);

        if (IsOwner)
        {
            Debug.Log($"I am {(isSeeker.Value ? "Seeker" : "Runner")}");
        }

        if (IsServer)
        {
            StartCoroutine(WaitForAllPlayersAndMove());
        }
    }

    private IEnumerator WaitForAllPlayersAndMove()
    {
        yield return new WaitUntil(() => NetworkManager.Singleton.ConnectedClientsList.Count == 2);
        MovePlayers();
    }

    private void MovePlayers()
    {
        foreach (var player in FindObjectsOfType<PlayerNetwork>())
        {
            if (player.IsOwner)
            {
                player.transform.position = IsHost ? new Vector3(-20.54f, 0f, -20.64f) : new Vector3(20.9441452f, 0f, 19.8427734f);
                player.rb.velocity = Vector3.zero;
            }
        }
    }

    [ClientRpc]
    public void AssignRoleClientRpc(bool isSeekerRole)
    {
        isSeeker.Value = isSeekerRole;
        Debug.Log(isSeekerRole ? "Assigned as Seeker" : "Assigned as Runner");
    }

    public void EnablePlayer()
    {
        isGameStarted = true;
    }

    private void FixedUpdate()
    {
        if (!isGameStarted || !IsOwner)
        {
            return;
        }

        Vector3 moveDir = Vector3.zero;
        if (Input.GetKey(KeyCode.W)) moveDir.z = +1;
        if (Input.GetKey(KeyCode.S)) moveDir.z = -1;
        if (Input.GetKey(KeyCode.A)) moveDir.x = -1;
        if (Input.GetKey(KeyCode.D)) moveDir.x = +1;

        MovePlayer(moveDir);
    }

    private void MovePlayer(Vector3 direction)
    {
        moveSpeed = isSeeker.Value ? seekerSpeed : runnerSpeed;

        Vector3 moveVector = direction.normalized * moveSpeed;
        rb.velocity = new Vector3(moveVector.x, rb.velocity.y, moveVector.z);

        if (moveVector.magnitude > rotationThreshold)
        {
            transform.rotation = Quaternion.LookRotation(moveVector);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!isGameStarted || !IsOwner) return;

        if (isSeeker.Value && collision.gameObject.CompareTag("Player"))
        {
            var playerNetwork = collision.gameObject.GetComponent<PlayerNetwork>();
            if (playerNetwork != null && !playerNetwork.isSeeker.Value)
            {
                Debug.Log("Seeker touched the runner. Awarding point to seeker.");
                GameManager.Instance.AwardPointToSeeker();
                GameManager.Instance.RestartTimer();
            }
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void IncreaseSpeedServerRpc(float speedIncrease, float duration)
    {
        IncreaseSpeedClientRpc(speedIncrease, duration);
    }

    [ClientRpc]
    private void IncreaseSpeedClientRpc(float speedIncrease, float duration)
    {
        StartCoroutine(SpeedBoost(speedIncrease, duration));
    }

    private IEnumerator SpeedBoost(float speedIncrease, float duration)
    {
        if (isSeeker.Value)
        {
            seekerSpeed += speedIncrease;
            Debug.Log($"Seeker speed boosted: {seekerSpeed}");
            yield return new WaitForSeconds(duration);
            seekerSpeed = originalSeekerSpeed;
            Debug.Log($"Seeker speed reverted: {seekerSpeed}");
        }
        else
        {
            runnerSpeed += speedIncrease;
            Debug.Log($"Runner speed boosted: {runnerSpeed}");
            yield return new WaitForSeconds(duration);
            runnerSpeed = originalRunnerSpeed;
            Debug.Log($"Runner speed reverted: {runnerSpeed}");
        }
    }

    [ClientRpc]
    public void ResetPositionClientRpc()
    {
        transform.position = IsHost ? new Vector3(-20.54f, 0f, -20.64f) : new Vector3(20.9441452f, 0f, 19.8427734f);
        rb.velocity = Vector3.zero;
    }
}
