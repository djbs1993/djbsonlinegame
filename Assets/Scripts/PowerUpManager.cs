using UnityEngine;
using Unity.Netcode;
using UnityEngine.SceneManagement;
using System.Collections;

public class PowerUpManager : MonoBehaviour
{
    public static PowerUpManager Instance { get; private set; }

    public GameObject[] powerUpPrefabs;
    public float spawnInterval = 10f;
    public Transform[] spawnPoints;

    private Coroutine spawnCoroutine;

    private void Awake()
    {
        Debug.Log("PowerUpManager Awake");
        Instance = this;
    }

    private void Start()
    {
        Debug.Log("PowerUpManager Start");
        InitializeNetworkCallbacks();
    }

    private void OnEnable()
    {
        Debug.Log("PowerUpManager OnEnable");
        SceneManager.sceneLoaded += OnSceneLoaded;
        InitializeNetworkCallbacks();
    }

    private void OnDisable()
    {
        Debug.Log("PowerUpManager OnDisable");
        SceneManager.sceneLoaded -= OnSceneLoaded;
        if (NetworkManager.Singleton != null)
        {
            NetworkManager.Singleton.OnServerStarted -= OnServerStarted;
            NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
            NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnected;
        }
    }

    private void InitializeNetworkCallbacks()
    {
        if (NetworkManager.Singleton != null)
        {
            NetworkManager.Singleton.OnServerStarted += OnServerStarted;
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
            NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnected;

            if (NetworkManager.Singleton.IsServer)
            {
                CheckAndStartPowerUpSpawning();
            }
        }
        else
        {
            Debug.LogError("NetworkManager.Singleton is null. Ensure a NetworkManager is in the scene.");
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log($"Scene loaded: {scene.name}");
        if (scene.name == "GameScene" || scene.name == "GameScene2")
        {
            CheckAndStartPowerUpSpawning();
        }
    }

    private void OnServerStarted()
    {
        Debug.Log("Server or Host started.");
        CheckAndStartPowerUpSpawning();
    }

    private void OnClientConnected(ulong clientId)
    {
        Debug.Log($"Client connected: {clientId}");
        CheckAndStartPowerUpSpawning();
    }

    private void OnClientDisconnected(ulong clientId)
    {
        Debug.Log($"Client disconnected: {clientId}");
        if (spawnCoroutine != null && NetworkManager.Singleton.ConnectedClients.Count < 2)
        {
            StopCoroutine(spawnCoroutine);
            spawnCoroutine = null;
        }
    }

    private void CheckAndStartPowerUpSpawning()
    {
        if (NetworkManager.Singleton.IsServer && NetworkManager.Singleton.ConnectedClients.Count >= 2)
        {
            if (spawnCoroutine == null)
            {
                Debug.Log("Starting power-up spawn coroutine on server.");
                spawnCoroutine = StartCoroutine(SpawnPowerUps());
            }
        }
    }

    private IEnumerator SpawnPowerUps()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnInterval);
            Debug.Log("Attempting to spawn power-up.");
            if (NetworkManager.Singleton.IsServer)
            {
                Debug.Log("Spawning power-up on server.");
                SpawnPowerUpServerRpc();
            }
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void SpawnPowerUpServerRpc()
    {
        if (spawnPoints.Length == 0 || powerUpPrefabs.Length == 0)
        {
            Debug.LogError("No spawn points or power-up prefabs set.");
            return;
        }

        
        int randomSpawnIndex = Random.Range(0, spawnPoints.Length);
        Transform spawnPoint = spawnPoints[randomSpawnIndex];

       
        int randomPowerUpIndex = Random.Range(0, powerUpPrefabs.Length);
        GameObject powerUpPrefab = powerUpPrefabs[randomPowerUpIndex];

        Debug.Log($"Spawning power-up at position: {spawnPoint.position}, prefab: {powerUpPrefab.name}");

       
        GameObject powerUp = Instantiate(powerUpPrefab, spawnPoint.position, Quaternion.identity);
        Debug.Log("Power-up instantiated.");

        
        NetworkObject networkObject = powerUp.GetComponent<NetworkObject>();
        if (networkObject != null)
        {
            networkObject.Spawn(true);
            Debug.Log("Power-up spawned successfully.");
        }
        else
        {
            Debug.LogError("Power-up prefab does not have a NetworkObject component.");
            Destroy(powerUp);
        }
    }

    public void ClearOldPowerUps()
    {
        if (NetworkManager.Singleton.IsServer)
        {
            var oldPowerUps = GameObject.FindGameObjectsWithTag("PowerUp");
            foreach (var powerUp in oldPowerUps)
            {
                NetworkObject networkObject = powerUp.GetComponent<NetworkObject>();
                if (networkObject != null && networkObject.IsSpawned)
                {
                    networkObject.Despawn(true);
                }
                else
                {
                    Destroy(powerUp);
                }
            }
        }
    }

    private void OnDestroy()
    {
        Debug.Log("PowerUpManager OnDestroy");
        if (NetworkManager.Singleton != null)
        {
            NetworkManager.Singleton.OnServerStarted -= OnServerStarted;
            NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
            NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnected;
        }
    }
}
