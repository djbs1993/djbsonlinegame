using Unity.Netcode;
using UnityEngine;

public class WallPowerUp : NetworkBehaviour
{
    public GameObject wallPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var playerNetwork = other.GetComponent<PlayerNetwork>();
            if (playerNetwork != null && playerNetwork.IsOwner)
            {
                TriggerWallServerRpc(playerNetwork.NetworkObjectId);
                RequestDespawnServerRpc();
            }
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void TriggerWallServerRpc(ulong playerId)
    {
        var player = NetworkManager.Singleton.SpawnManager.SpawnedObjects[playerId];
        if (player != null)
        {
            var playerTransform = player.transform;
            Vector3 wallPosition = playerTransform.position - playerTransform.forward * 2;
            var wall = Instantiate(wallPrefab, wallPosition, Quaternion.identity);
            NetworkObject networkObject = wall.GetComponent<NetworkObject>();
            if (networkObject != null)
            {
                networkObject.Spawn(true);
                Destroy(wall, 5f);
            }
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void RequestDespawnServerRpc()
    {
        if (NetworkObject != null && IsServer)
        {
            NetworkObject.Despawn(true);
        }
    }
}
