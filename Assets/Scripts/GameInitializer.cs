using UnityEngine;
using Unity.Netcode;
using UnityEngine.SceneManagement;

public class GameInitializer : MonoBehaviour
{
    private void Start()
    {
        if (NetworkState.Instance != null)
        {
            if (NetworkState.Instance.IsHost)
            {
                if (!NetworkManager.Singleton.IsServer && !NetworkManager.Singleton.IsClient)
                {
                    NetworkManager.Singleton.StartHost();
                }
                else
                {
                    Debug.LogWarning("Host is already running");
                }
            }
            else
            {
                if (!NetworkManager.Singleton.IsClient && !NetworkManager.Singleton.IsServer)
                {
                    NetworkManager.Singleton.StartClient();
                }
                else
                {
                    Debug.LogWarning("Client is already running");
                }
            }
        }
        else
        {
            Debug.LogError("NetworkState instance is null in GameInitializer");
        }
    }

    public void ReturnToMainMenu()
    {
 
        CleanUpNetworkObjects();

        if (NetworkManager.Singleton.IsHost || NetworkManager.Singleton.IsClient)
        {
            NetworkManager.Singleton.Shutdown();
        }

        SceneManager.LoadScene("MainMenuScene");
    }

    private void CleanUpNetworkObjects()
    {

        var networkObjects = FindObjectsOfType<NetworkObject>();
        foreach (var netObj in networkObjects)
        {
            if (netObj.IsSpawned)
            {
                netObj.Despawn(true);
            }
        }
    }
}
