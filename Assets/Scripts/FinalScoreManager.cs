using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using Unity.Netcode;
using System.Collections;
using System.Collections.Generic;

public class FinalScoreManager : MonoBehaviour
{
    public static FinalScoreManager Instance { get; private set; }

    public TMP_Text seekerScoreText;
    public TMP_Text runnerScoreText;
    public GameObject returnToMainMenuButton;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        DisplayFinalScores();

        returnToMainMenuButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(OnReturnToMainMenuClicked);
    }

    public void DisplayFinalScores()
    {
        if (PersistentGameManager.Instance != null)
        {
            seekerScoreText.text = $"Seeker Total Score: {PersistentGameManager.Instance.seekerTotalScore.Value}";
            runnerScoreText.text = $"Runner Total Score: {PersistentGameManager.Instance.runnerTotalScore.Value}";
        }
        else
        {
            Debug.LogError("PersistentGameManager.Instance is null. Cannot display final scores.");
        }
    }

    public void OnReturnToMainMenuClicked()
    {
        StartCoroutine(LogoutAndReturnToMainMenu());
    }

    private IEnumerator LogoutAndReturnToMainMenu()
    {
        if (NetworkManager.Singleton != null)
        {
            if (NetworkManager.Singleton.IsServer)
            {
                foreach (var clientId in NetworkManager.Singleton.ConnectedClientsIds)
                {
                    if (clientId != NetworkManager.Singleton.LocalClientId)
                    {
                        NetworkManager.Singleton.DisconnectClient(clientId);
                    }
                }
            }

            NetworkManager.Singleton.Shutdown();
        }

        yield return new WaitForEndOfFrame();

        CleanUpDontDestroyOnLoad();

        NetworkManager.Singleton.Shutdown();
        yield return new WaitForEndOfFrame();

        SceneManager.LoadScene("MainMenuScene");
    }

    private void CleanUpDontDestroyOnLoad()
    {
        var dontDestroyonLoadObjects = new List<GameObject>();
        var dontDestroyonLoadScene = SceneManager.GetSceneByName("DontDestroyOnLoad");
        if (dontDestroyonLoadScene.isLoaded)
        {
            foreach (var rootGameObject in dontDestroyonLoadScene.GetRootGameObjects())
            {
                dontDestroyonLoadObjects.Add(rootGameObject);
            }
        }

        foreach (var obj in dontDestroyonLoadObjects)
        {
            Destroy(obj);
        }
    }
}
