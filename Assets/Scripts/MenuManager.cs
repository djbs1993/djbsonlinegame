using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private Button hostButton;
    [SerializeField] private Button joinButton;
    [SerializeField] private TextMeshProUGUI statusText;

    private void Start()
    {
        hostButton.onClick.AddListener(OnHostButtonClicked);
        joinButton.onClick.AddListener(OnJoinButtonClicked);
    }

    private void OnHostButtonClicked()
    {
        NetworkState.Instance.IsHost = true;
        statusText.text = "Starting host...";
        SceneManager.LoadScene("GameScene");
    }

    private void OnJoinButtonClicked()
    {
        NetworkState.Instance.IsHost = false;
        statusText.text = "Joining game...";
        SceneManager.LoadScene("GameScene");
    }
}
