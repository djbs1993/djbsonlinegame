using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;
using UnityEngine.SceneManagement;
using System.Collections;

public class PauseMenuManager : MonoBehaviour
{
    public GameObject pauseMenuCanvas;
    public Button resumeButton;
    public Button logoutButton;

    private void Start()
    {
        pauseMenuCanvas.SetActive(false);

        resumeButton.onClick.AddListener(ResumeGame);
        logoutButton.onClick.AddListener(Logout);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pauseMenuCanvas.activeSelf)
            {
                ResumeGame();
            }
            else
            {
                ShowPauseMenu();
            }
        }
    }

    private void ShowPauseMenu()
    {
        pauseMenuCanvas.SetActive(true);
    }

    public void ResumeGame()
    {
        pauseMenuCanvas.SetActive(false);
    }

    public void Logout()
    {
        if (NetworkManager.Singleton.IsHost)
        {
            NotifyClientsToReturnToMainMenuClientRpc();
            StartCoroutine(HostLogoutAndReturnToMainMenu());
        }
        else if (NetworkManager.Singleton.IsClient)
        {
            StartCoroutine(ClientLogoutAndReturnToMainMenu());
        }
        else
        {
            DirectReturnToMainMenu();
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void RequestLogoutServerRpc()
    {
        NotifyClientsToReturnToMainMenuClientRpc();
        StartCoroutine(HostLogoutAndReturnToMainMenu());
    }

    [ClientRpc]
    private void NotifyClientsToReturnToMainMenuClientRpc()
    {
        if (!NetworkManager.Singleton.IsHost)
        {
            StartCoroutine(ClientReturnToMainMenu());
        }
    }

    private IEnumerator HostLogoutAndReturnToMainMenu()
    {

        yield return new WaitForSeconds(0.5f);

        if (NetworkManager.Singleton.IsServer)
        {
            CleanUpNetworkObjects();
        }

        NetworkManager.Singleton.Shutdown();

        yield return new WaitForSeconds(1f);


        SceneManager.LoadScene("MainMenuScene");
    }

    private IEnumerator ClientLogoutAndReturnToMainMenu()
    {
        if (NetworkManager.Singleton.IsClient && NetworkManager.Singleton.IsConnectedClient)
        {

            RequestLogoutServerRpc();
            yield return new WaitForSeconds(0.5f);
        }


        yield return ClientReturnToMainMenu();
    }

    private IEnumerator ClientReturnToMainMenu()
    {

        yield return new WaitForSeconds(0.5f);


        if (NetworkManager.Singleton.IsConnectedClient)
        {
            NetworkManager.Singleton.Shutdown();
        }

        yield return new WaitForSeconds(1f);


        SceneManager.LoadScene("MainMenuScene");
    }

    private void DirectReturnToMainMenu()
    {

        SceneManager.LoadScene("MainMenuScene");
    }

    private void CleanUpNetworkObjects()
    {

        if (NetworkManager.Singleton.IsServer)
        {
            var networkObjects = FindObjectsOfType<NetworkObject>();
            foreach (var netObj in networkObjects)
            {
                if (netObj.IsSpawned)
                {
                    netObj.Despawn(true);
                }
            }
        }
    }
}
