using UnityEngine;
using Unity.Netcode;

public class PersistentGameManager : NetworkBehaviour
{
    public static PersistentGameManager Instance { get; private set; }

    public NetworkVariable<int> seekerTotalScore = new NetworkVariable<int>(0);
    public NetworkVariable<int> runnerTotalScore = new NetworkVariable<int>(0);

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void AddSeekerScoreServerRpc()
    {
        seekerTotalScore.Value++;
    }

    [ServerRpc(RequireOwnership = false)]
    public void AddRunnerScoreServerRpc()
    {
        runnerTotalScore.Value++;
    }

    [ServerRpc(RequireOwnership = false)]
    public void ResetTotalScoresServerRpc()
    {
        seekerTotalScore.Value = 0;
        runnerTotalScore.Value = 0;
    }
}
