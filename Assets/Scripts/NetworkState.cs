using UnityEngine;

public class NetworkState : MonoBehaviour
{
    public static NetworkState Instance { get; private set; }
    public bool IsHost { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
