using UnityEngine;
using Unity.Netcode;

public class SpeedPowerUp : NetworkBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var playerNetwork = other.GetComponent<PlayerNetwork>();
            if (playerNetwork != null && playerNetwork.IsOwner)
            {
                playerNetwork.IncreaseSpeedServerRpc(5f, 5f);
                RequestDespawnServerRpc();
            }
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void RequestDespawnServerRpc()
    {
        if (NetworkObject != null)
        {
            NetworkObject.Despawn();
        }
    }
}
