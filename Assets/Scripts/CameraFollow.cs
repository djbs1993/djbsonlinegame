using UnityEngine;
using Unity.Netcode;

public class CameraFollow : MonoBehaviour
{
    private Transform player;
    public float height = 15f;
    public float smoothSpeed = 0.125f;

    void Start()
    {

        if (NetworkManager.Singleton != null && NetworkManager.Singleton.IsClient)
        {
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
            FindLocalPlayer();
        }


        ResetCameraPosition();
    }

    private void OnClientConnected(ulong clientId)
    {
        if (clientId == NetworkManager.Singleton.LocalClientId)
        {
            FindLocalPlayer();
        }
    }

    void FindLocalPlayer()
    {
        foreach (var obj in GameObject.FindGameObjectsWithTag("Player"))
        {
            var networkObject = obj.GetComponent<NetworkObject>();
            if (networkObject != null && networkObject.IsLocalPlayer)
            {
                player = obj.transform;
                break;
            }
        }
    }

    void LateUpdate()
    {
        if (player != null)
        {

            Vector3 desiredPosition = player.position + new Vector3(0, height, 0);
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;


            transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        }
        else
        {

            FindLocalPlayer();
        }
    }

    public void ResetCameraPosition()
    {
        if (player != null)
        {

            transform.position = player.position + new Vector3(0, height, 0);

            transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        }
    }
}
