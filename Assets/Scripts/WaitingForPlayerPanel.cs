using UnityEngine;
using Unity.Netcode;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class WaitingForPlayerPanel : MonoBehaviour
{
    private GameManager gameManager;
    public Button goToMainMenuButton;

    private void Start()
    {
        gameManager = GameManager.Instance;
        if (goToMainMenuButton != null)
        {
            goToMainMenuButton.onClick.AddListener(OnGoToMainMenuButtonClicked);
        }
    }

    private void OnGoToMainMenuButtonClicked()
    {
        if (NetworkManager.Singleton != null && NetworkManager.Singleton.IsClient)
        {
            gameManager.RequestDisconnectServerRpc();
            StartCoroutine(ClientLogoutAndReturnToMainMenu());
        }
        else if (NetworkManager.Singleton != null && NetworkManager.Singleton.IsHost)
        {
            StartCoroutine(gameManager.HostLogoutAndReturnToMainMenu());
        }
    }

    private IEnumerator ClientLogoutAndReturnToMainMenu()
    {
        if (NetworkManager.Singleton.IsClient && NetworkManager.Singleton.IsConnectedClient)
        {
            gameManager.RequestDisconnectServerRpc();
            yield return new WaitForSeconds(0.5f);
        }

        yield return ClientReturnToMainMenu();
    }

    private IEnumerator ClientReturnToMainMenu()
    {
        yield return new WaitForSeconds(0.5f);

        if (NetworkManager.Singleton.IsConnectedClient)
        {
            NetworkManager.Singleton.Shutdown();
        }

        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene("MainMenuScene");
    }
}
