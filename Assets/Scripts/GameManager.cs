using UnityEngine;
using Unity.Netcode;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : NetworkBehaviour
{
    public static GameManager Instance { get; private set; }

    public NetworkVariable<int> connectedPlayers = new NetworkVariable<int>(0);
    public NetworkVariable<int> seekerScore = new NetworkVariable<int>(0);
    public NetworkVariable<int> runnerScore = new NetworkVariable<int>(0);
    public NetworkVariable<float> timer = new NetworkVariable<float>(10f);

    private GameObject waitingCanvas;
    private bool gameStarted = false;
    private bool isShuttingDown = false;
    private bool scoresReset = false;
    private const int pointsToWin = 6;
    private Coroutine timerCoroutine;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        Debug.Log("GameManager initialized");
    }

    private void Start()
    {
        InitializeGameManager();
    }

    private void InitializeGameManager()
    {

    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log($"Scene loaded: {scene.name}");

        if (scene.name == "GameScene")
        {
            scoresReset = false;
            StartCoroutine(ResetScoresAfterSceneLoad());
            FindWaitingCanvas();
        }
        else if (scene.name == "MainMenuScene")
        {
            ResetScores();
        }
        else if (scene.name == "GameScene2" || scene.name == "GameScene3")
        {
            RestartTimer();
        }

        StartCoroutine(ReinitializeReferences());
    }

    private IEnumerator ResetScoresAfterSceneLoad()
    {
        yield return new WaitForEndOfFrame();

        if (IsServer && !scoresReset)
        {
            seekerScore.Value = 0;
            runnerScore.Value = 0;
            scoresReset = true;

            if (PersistentGameManager.Instance != null)
            {
                PersistentGameManager.Instance.ResetTotalScoresServerRpc();
            }
        }

        if (IsServer)
        {
            PlayerManager.Instance.SpawnPlayers();
        }
    }

    private void FindWaitingCanvas()
    {
        waitingCanvas = GameObject.Find("WaitingCanvas");

        if (waitingCanvas == null)
        {
            Debug.LogError("WaitingCanvas not found in the scene.");
        }
        else
        {
            if (connectedPlayers.Value < 2)
            {
                ShowWaitingCanvas();
            }
            else
            {
                HideWaitingCanvas();
            }
        }
    }

    private void ResetScores()
    {
        seekerScore.Value = 0;
        runnerScore.Value = 0;

        if (PersistentGameManager.Instance != null)
        {
            PersistentGameManager.Instance.ResetTotalScoresServerRpc();
        }
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (IsServer)
        {
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
            NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnected;
            UpdateConnectedPlayers();
        }

        if (IsHost && connectedPlayers.Value < 2)
        {
            ShowWaitingCanvas();
        }
        else
        {
            HideWaitingCanvas();
        }
    }

    private void OnClientConnected(ulong clientId)
    {
        UpdateConnectedPlayers();

        if (IsServer && connectedPlayers.Value == 2)
        {
            AssignRoles();
            MovePlayers();
        }
    }

    private void OnClientDisconnected(ulong clientId)
    {
        UpdateConnectedPlayers();
    }

    private void UpdateConnectedPlayers()
    {
        if (isShuttingDown) return;

        if (NetworkManager.Singleton != null)
        {
            connectedPlayers.Value = NetworkManager.Singleton.ConnectedClientsList.Count;
            CheckStartGame();
        }

        if (IsHost && connectedPlayers.Value >= 2)
        {
            HideWaitingCanvas();
        }
        else if (IsHost && connectedPlayers.Value < 2)
        {
            ShowWaitingCanvas();
        }
    }

    private void CheckStartGame()
    {
        if (connectedPlayers.Value >= 2 && !gameStarted)
        {
            StartGame();
        }
    }

    private void StartGame()
    {
        gameStarted = true;
        StartGameClientRpc();
        if (IsServer)
        {
            StartTimer();
        }
    }

    [ClientRpc]
    private void StartGameClientRpc()
    {
        foreach (var player in FindObjectsOfType<PlayerNetwork>())
        {
            player.EnablePlayer();
        }
    }

    private void StartTimer()
    {
        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
        }
        timerCoroutine = StartCoroutine(TimerCoroutine());
    }

    private IEnumerator TimerCoroutine()
    {
        while (gameStarted)
        {
            if (isShuttingDown) yield break;

            timer.Value -= Time.deltaTime;

            if (timer.Value <= 0)
            {
                RestartTimer();
                AwardPointToRunner();
            }

            UpdateTimerClientRpc(timer.Value);
            yield return null;
        }
    }

    public void AwardPointToSeeker()
    {
        if (isShuttingDown) return;

        seekerScore.Value++;
        PersistentGameManager.Instance.AddSeekerScoreServerRpc();
        UpdateScoresClientRpc(seekerScore.Value, runnerScore.Value);
        CheckForLevelTransition();
    }

    public void AwardPointToRunner()
    {
        if (isShuttingDown) return;

        runnerScore.Value++;
        PersistentGameManager.Instance.AddRunnerScoreServerRpc();
        UpdateScoresClientRpc(seekerScore.Value, runnerScore.Value);
        CheckForLevelTransition();
    }

    private void CheckForLevelTransition()
    {
        if (seekerScore.Value >= pointsToWin || runnerScore.Value >= pointsToWin)
        {
            if (SceneManager.GetActiveScene().name == "GameScene3")
            {
                gameStarted = false;
                TransitionToFinalScoreScene();
            }
            else if (SceneManager.GetActiveScene().name == "GameScene2")
            {
                StartCoroutine(TransitionToNextLevel("GameScene3"));
            }
            else
            {
                StartCoroutine(TransitionToNextLevel("GameScene2"));
            }
        }
    }

    private void TransitionToFinalScoreScene()
    {
        gameStarted = false;
        TransitionToFinalScoreSceneClientRpc();
        StartCoroutine(LoadFinalScoreScene());
    }

    [ClientRpc]
    private void TransitionToFinalScoreSceneClientRpc()
    {
        StartCoroutine(LoadFinalScoreSceneClient());
    }

    private IEnumerator LoadFinalScoreScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("FinalScoreScene", LoadSceneMode.Single);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        NetworkManager.Singleton.Shutdown();
        CleanUpDontDestroyOnLoad();
    }

    private IEnumerator LoadFinalScoreSceneClient()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("FinalScoreScene", LoadSceneMode.Single);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        if (FinalScoreManager.Instance != null)
        {
            FinalScoreManager.Instance.DisplayFinalScores();
        }
    }

    private IEnumerator TransitionToNextLevel(string nextScene)
    {
        gameStarted = false;
        TransitionToNextLevelClientRpc(nextScene);

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(nextScene, LoadSceneMode.Single);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        yield return new WaitForEndOfFrame();

        ResetScoresAndTimer();

        ReenableAndResetPlayers();

        UpdateConnectedPlayers();

        if (IsServer)
        {
            StartTimer();
        }
    }

    private void ResetScoresAndTimer()
    {
        seekerScore.Value = 0;
        runnerScore.Value = 0;
        timer.Value = 10f;
        UpdateTimerClientRpc(timer.Value);
    }

    private void ReenableAndResetPlayers()
    {
        foreach (var player in FindObjectsOfType<PlayerNetwork>())
        {
            player.EnablePlayer();
        }
        ResetPlayerPositionsServerRpc();
    }

    [ServerRpc]
    private void ResetPlayerPositionsServerRpc()
    {
        foreach (var player in FindObjectsOfType<PlayerNetwork>())
        {
            player.ResetPositionClientRpc();
        }
    }

    [ClientRpc]
    private void TransitionToNextLevelClientRpc(string nextScene)
    {
        StartCoroutine(LoadNewSceneOnClients(nextScene));
    }

    private IEnumerator LoadNewSceneOnClients(string nextScene)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(nextScene);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        ResetCamera();
        UpdateTimerClientRpc(timer.Value);
    }

    private void ResetCamera()
    {
        CameraFollow cameraFollow = FindObjectOfType<CameraFollow>();
        if (cameraFollow != null)
        {
            cameraFollow.ResetCameraPosition();
        }
    }

    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        if (IsServer)
        {
            NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
            NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnected;
        }
    }

    public void RestartTimer()
    {
        if (isShuttingDown) return;

        timer.Value = 10f;
        UpdateTimerClientRpc(timer.Value);
        if (IsServer)
        {
            StartTimer();
        }
    }

    [ClientRpc]
    private void UpdateScoresClientRpc(int newSeekerScore, int newRunnerScore)
    {

    }

    [ClientRpc]
    private void UpdateTimerClientRpc(float newTime)
    {
        if (!IsServer)
        {
            timer.Value = newTime;
        }
    }

    private void ShowWaitingCanvas()
    {
        if (waitingCanvas != null)
        {
            waitingCanvas.SetActive(true);
        }
    }

    private void HideWaitingCanvas()
    {
        if (waitingCanvas != null)
        {
            waitingCanvas.SetActive(false);
        }
    }

    private void CleanUpDontDestroyOnLoad()
    {
        var dontDestroyonLoadObjects = new List<GameObject>();
        var dontDestroyonLoadScene = SceneManager.GetSceneByName("DontDestroyOnLoad");
        if (dontDestroyonLoadScene.isLoaded)
        {
            foreach (var rootGameObject in dontDestroyonLoadScene.GetRootGameObjects())
            {
                dontDestroyonLoadObjects.Add(rootGameObject);
            }
        }

        foreach (var obj in dontDestroyonLoadObjects)
        {
            Destroy(obj);
        }
    }

    private IEnumerator ReinitializeReferences()
    {
        yield return new WaitForEndOfFrame();

        waitingCanvas = GameObject.Find("WaitingCanvas");

        if (waitingCanvas == null)
        {
            Debug.LogError("WaitingCanvas not found in the scene.");
        }
        else
        {
            if (connectedPlayers.Value < 2)
            {
                ShowWaitingCanvas();
            }
            else
            {
                HideWaitingCanvas();
            }
        }
    }

    private void AssignRoles()
    {
        ulong[] clientIds = new ulong[2];
        int index = 0;

        foreach (var clientId in NetworkManager.Singleton.ConnectedClientsIds)
        {
            clientIds[index] = clientId;
            index++;
        }

        var hostPlayer = NetworkManager.Singleton.ConnectedClients[clientIds[0]].PlayerObject.GetComponent<PlayerNetwork>();
        var clientPlayer = NetworkManager.Singleton.ConnectedClients[clientIds[1]].PlayerObject.GetComponent<PlayerNetwork>();

        hostPlayer.AssignRoleClientRpc(true);
        clientPlayer.AssignRoleClientRpc(false);
    }

    private void MovePlayers()
    {
        foreach (var player in FindObjectsOfType<PlayerNetwork>())
        {
            if (player.IsOwner)
            {
                player.transform.position = player.IsHost ? new Vector3(-20.54f, 0f, -20.64f) : new Vector3(20.9441452f, 0f, 19.8427734f);
                player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
    }

    public void OnMainMenuButtonClicked()
    {
        StartCoroutine(LogoutAndReturnToMainMenu());
    }

    private IEnumerator LogoutAndReturnToMainMenu()
    {
        if (NetworkManager.Singleton != null)
        {
            if (IsServer)
            {
                foreach (var clientId in NetworkManager.Singleton.ConnectedClientsIds)
                {
                    if (clientId != NetworkManager.Singleton.LocalClientId)
                    {
                        NetworkManager.Singleton.DisconnectClient(clientId);
                    }
                }
            }

            NetworkManager.Singleton.Shutdown();
        }

        yield return new WaitForEndOfFrame();

        CleanUpDontDestroyOnLoad();

        NetworkManager.Singleton.Shutdown();
        yield return new WaitForEndOfFrame();

        SceneManager.LoadScene("MainMenuScene");
    }

    [ServerRpc]
    public void RequestDisconnectServerRpc()
    {
        connectedPlayers.Value--;
        UpdateConnectedPlayers();
    }

    public IEnumerator HostLogoutAndReturnToMainMenu()
    {
        yield return new WaitForSeconds(0.5f);

        if (NetworkManager.Singleton.IsServer)
        {
            CleanUpNetworkObjects();
        }

        NetworkManager.Singleton.Shutdown();

        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene("MainMenuScene");
    }

    private void CleanUpNetworkObjects()
    {
        var networkObjects = FindObjectsOfType<NetworkObject>();
        foreach (var netObj in networkObjects)
        {
            if (netObj.IsSpawned)
            {
                netObj.Despawn(true);
            }
        }
    }
}
