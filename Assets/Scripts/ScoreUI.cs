using UnityEngine;
using TMPro;
using Unity.Netcode;

public class ScoreUI : NetworkBehaviour
{
    [SerializeField] private TextMeshProUGUI seekerScoreText;
    [SerializeField] private TextMeshProUGUI runnerScoreText;
    [SerializeField] private TextMeshProUGUI timerText;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.Instance;

        if (gameManager != null)
        {
            gameManager.seekerScore.OnValueChanged += UpdateSeekerScoreText;
            gameManager.runnerScore.OnValueChanged += UpdateRunnerScoreText;
            gameManager.timer.OnValueChanged += UpdateTimerText;
        }

        UpdateSeekerScoreText(0, gameManager.seekerScore.Value);
        UpdateRunnerScoreText(0, gameManager.runnerScore.Value);
        UpdateTimerText(0, gameManager.timer.Value);
    }

    private void UpdateSeekerScoreText(int previousValue, int newValue)
    {
        if (seekerScoreText != null)
        {
            seekerScoreText.text = "Seeker Score: " + newValue.ToString();
        }
    }

    private void UpdateRunnerScoreText(int previousValue, int newValue)
    {
        if (runnerScoreText != null)
        {
            runnerScoreText.text = "Runner Score: " + newValue.ToString();
        }
    }

    private void UpdateTimerText(float previousValue, float newValue)
    {
        if (timerText != null)
        {
            timerText.text = "Time Left: " + newValue.ToString("F2");
        }
    }
}
